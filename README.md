#Petclinic example project
This project is a working example of how integrate [Behave Pro for JIRA](https://www.hindsightsoftware.com/behave-pro) with Cucumber-JVM and WebDriver using Maven.

##Prerequisites
You will require the following software to be installed
* [Git](https://git-scm.com/)
* [Apache Maven](https://maven.apache.org/)
* [Firefox webrowser](https://www.mozilla.org/en-US/firefox/new/)
* [GeckoDriver](https://github.com/mozilla/geckodriver/releases) - You will need the correct GeckoDriver version that coresponds to the version of Firefox you have installed

## Preparing the example
You will only need todo this setup once.

Open a terminal or command prompt in the location you want to setup the example project

`git clone https://bitbucket.org/hindsightsoftwareltd/behave-pet-clinic-sample.git`

`cd behave-pet-clinic-sample`

`mvn package`


## Running the example
The example will start a firefox web browsers using Selenium WebDriver. You will need to run GeckkoDriver for the automated scenario to access Firefox

Open a terminal or command prompt in the `acceptance` folder of the example project

`mvn verify`

You will see Maven start the application under test, the behave maven plugin export feature files from Behave Pro, run Cucumber using the failsafe plugin, and the shutdown the application under test



