package com.hindsighttesting.samples.petclinic.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;

public class Steps {

    private WebDriver driver;

    @Before
    public void startWebDriver() throws Exception {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/"), capabilities);
    }

    @After
    public void closeWebDriver() {
        if (driver != null) {
            // WebDriver did load
            driver.quit();
        }
    }

    @Given("^a registered owner$")
    public void a_registered_owner() {

    }

    @Given("^an owner with a registered pet called \"([^\"]*)\"$")
    public void an_owner_with_a_registered_pet_called(String name) {

    }

    @When("^the owner registers a new pet called \"([^\"]*)\"$")
    public void the_owner_registers_a_new_pet_called(String name) {
        driver.get("http://localhost:8080/petclinic/owners/2.html");
        driver.findElement(By.cssSelector("a.btn-success")).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("name"))).sendKeys(name);

        driver.findElement(By.id("birthDate")).sendKeys("2013/03/03");
        driver.findElement(By.id("type")).findElement(By.cssSelector("option[value='hamster']"))
                .click();
        driver.findElement(By.id("pet")).submit();
    }

    @Then("^the user should be warned that the pet already exists$")
    public void the_user_should_be_warned_that_the_pet_already_exists() {

    }


}
