package com.hindsighttesting.samples.petclinic;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(format = { "json:target/cucumber.json", "html:target/reports" }, tags = { "~@OPEN" }, features = { "target/generated-test-resources" }, strict = false)
public class CucumberIT {

}
